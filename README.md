STM32 Development Tools
=======================

What's in the box
-----------------

The `stm32-buildimg` image contains:
  - a binary distribution of ARM gcc version 10
  - a current [stlink](https://github.com/stlink-org/stlink) devlop branch build
  - a current [BlackMagic](https://github.com/blacksphere/blackmagic) build, hosted mode


How to use the STM32 Tools Image
--------------------------------

The `stm32-buildimg/stm32-run` script contains an example of how to run the
image using podman. The home directory of the calling user is mapped inside
the container, so STM32 projects with `$HOME` can be accessed seamlessly
by the same path.

You can use `stm32-run` either interactively or directly, e.g. to start
a shell:
```
    $ stm32-run bash
```

or to run a command:
```
    $ stm32-run gcc ...
```

To access the `/dev/stlink*` devices needed for communication with
an ST-LINK proble the user inside the container needs to be `root`.
This sometimes botches up file permissions on the host system :-(
If anyone is aware of a more elegant solution, please feel free
to contact me.
